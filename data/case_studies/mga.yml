title: MGA
cover_image: '/images/blogimages/covermga.jpg'
cover_title: |
  How MGA builds projects 5 times faster with GitLab
cover_description: |
  MGA successfully implemented CI/CD, improved software quality, created a knowledge base, and saved money by migrating to GitLab.
twitter_image: '/images/blogimages/covermga.jpg'

twitter_text: 'MGA implemented CI/CD, improved software quality, created a knowledge base, and saved money by migrating to GitLab.'

customer_logo: '/images/case_study_logos/logo-mga.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Poland
customer_solution: GitLab EE
customer_employees: 80 employees
customer_overview: |
  MGA migrated to GitLab and created better software faster with automated CI and CD.
customer_challenge: |
  MGA was looking for a cost efficient CI platform that could improve workflow, knowledge, and code quality.

key_benefits:
  - |
    Automatic code scanners in CI pipeline
  - |
    Improved overall collaboration
  - |
    Increased operational efficiency
  - |
    Deliver better products
  - |
    Easy to integrate

customer_stats:
  - stat: 80 to 240
    label: Projects growth rate
  - stat: 10
    label: Times better success rate with CD than manual deploy
  - stat: 80%
    label: Time saved moving to CD

customer_study_content:
  - title: the customer
    subtitle: Logical software enterprise
    content:
      - |
        MGA designs, establishes, and implements computer applications that work with large and medium sized commercial and industrial enterprises. Founded in 1993, MGA created logical software that leverages the advantages of a relational database (Oracle) working on the Linux operating system. By 1999, [MGA](https://mga.com.pl/en/) started outsourcing and began providing bookkeeping services, human resource support and payroll services to over 30 companies. 

  - title: the challenge
    subtitle: Lacking collaboration, support, and code quality
    content:
      - |
        MGA was using Mercurial and their teams wrote their own code. The development team tested out free tools that allowed them to make code reviews that enabled CI and CD. It was a difficult process because they were not experienced in using the tools, and there was a lack of support in getting up and running. Between Mercurial being too complex and developers having inadequate experience with CI/CD tools, MGA was facing significant challenges.
      - |
        IT administrators were overwhelmed and overworked. The development team consisted of about 25 people with only three system administrators. Every IT problem that arose within the company fell to the IT admins to solve. There was no formal way to communicate between developers and IT, so bottlenecks were common. MGA was looking for a tool that would allow them to automate and streamline as many things as possible. They also required a platform that optimized collaboration in order to improve their existing [version control issues](/blog/2019/10/28/optimize-gitops-workflow/).
      - |
        Code quality suffered due to the inability to collaborate and share documentation. “We started to look at tools that would allow us to just make some code review and to create some approval policies for managing the codes. The second thing was to start with some CI/CD strategies just to make our work easier and to keep our administrator's team as small as possible,” said Jakub Tadeja, Senior IT Administrator.
      - |
        The teams were also struggling with knowledge distribution. MGA had articles stored in their internal Wikis, in Redmine tasks, written notes, and in some applications. Knowledge assets were siloed, unorganized, and hard to find. New hires were left on their own to find documentation to learn about the internal systems. MGA IT and developers were losing time and money without the proper tools to solve all their issues. 
        
  - title: the solution
    subtitle: A dynamic tool at a cost efficient price
    content:
      - |
        MGA tested out a variety of tools, but after discovering GitLab’s easy user interface, they made the decision to migrate from Mercurial. Another selling point for GitLab was [Deviniti](https://deviniti.com/), an accredited GitLab partner based in Poland. Within a week of working with this well established partner, MGA made the decision to move forward with GitLab. “MGA needed Polish currency and Polish invoicing to proceed with the procurement process. Most Polish customers are looking for a local partner who will be able to sell to them,” said Radosław Kosiec, Sales Director, Deviniti. Having Deviniti locally for support and domain expertise has really improved MGA’s adoption process and they remain very pleased with the partnership. “Working with Deviniti has been awesome and I would recommend working with them,” Tadeja added.
      - |
        GitLab’s pricing plan was the best option for MGA and was a huge driver in their decision-making process. “We don't earn as much as Western countries and Western companies...we were looking for a solution that will allow us to self cost that application. As far as I know, GitHub and other cloud solutions are not created that way. So it's impossible to use it on site,” Tadeja said.
      - |
        Before GitLab, MGA developers worked in small teams on small projects. But as the company grew, so did the scope of the projects and the size of the teams. The tools they had in place couldn’t accommodate the scaling. “GitLab completely changed the way we make our projects now,” Tadeja said. “We didn't expect much at the beginning. We just wanted something new that will allow us to just create some CI/CD and code review policies. That was all. We discovered GitLab offers a lot more and we started to play with the things and we liked it.”
      - |
        With GitLab, teams found the ability to plan a software roadmap and set timelines for projects. “We started to plan our projects using continuous integration and continuous deployment. GitLab completely changed the architecture of our solution,” Tadeja said.

  - blockquote: CD has at least 10 times better success rate than manual deploy. We have projects in which automatic deployment has never failed.
    attribution: Jakub Tadeja 
    attribution_title: Senior IT Administrator

  - title: the results
    subtitle: Better software, more developers, and a single knowledge base
    content:
      - |
        “Initial success: It just worked. That was the biggest success because we didn't have any problems with installing GitLab, with updating it and it was really simple to start for us,” according to Tadeja. MGA took about a week to prepare servers to start using GiLab. Within a year the teams had migrated all of their projects from the previous solution. “It was really easy to start and we didn’t have any issues,” Tadeja added.
      - |
        Prior to GitLab, there were three IT administrators and 30 developers. Now, there are over 60 developers easily supported by three IT admins and many more servers. There is no longer a bottleneck because the problems no longer fall on the IT admins. 
      - |
        Since starting with GitLab, projects have grown from 80 to 240. “Everything in projects that's already started is done by [CI and CD](/stages-devops-lifecycle/continuous-integration/). We just solve some problems from time to time and teach developers how to use the tools that GitLab provides,” Tadeja said. “We are more efficient, know how to write better code, and we can focus more on code quality than just simple tasks that can be automated.”
      - |
        The level of software quality is improved since the teams are able to enhance the quality of code. “I believe we provide much better code quality and the most important thing is that we can collaborate in creating codes for bigger things,” Tadeja said. “Now we can review our codes and text notes and we can share it...I believe it really simplifies our work.”
      - |
        Onboarding has become more efficient because MGA now documents everything as code. Developers can now look at a CI configuration and from a YML, they are able to learn how things work. Every developer knows where to look for knowledge, which saves time and headaches. “Our onboarding changed from teaching people for weeks and showing where the knowledge is, to showing them where the coffee maker is and how to get to the toilet,” Tadeja said.
      - |
        With GitLab’s support, MGA has created better software at a faster rate with improved code quality testing and review processes. The IT and development teams have become experts in CI/CD, creating streamlined automation systems, keeping IT administration resources at a minimum and maximizing cost efficiency.
